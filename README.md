# Docker Network Visualizer
This small app can be used to visualize your current Docker environment with all containers and networks. It is designed to provide a simpler view of proxy networks and open ports of a docker host.

(I know it's not that pretty, but I prefer functionality to design)
![](example/image.png)

## Features
* Show docker containers with image names and, if set, VIRTUAL_HOST, VIRTUAL_PORT and LETSENCRYPT_HOST (people using jwilder/nginx-proxy will know these env-vars)
* Displays docker networks with subnet and all connected containers
* Displays published ports for each container
* Display clickable url of VIRTUAL_HOST for each container (first url will be used if multiple are set)
* Cache for created svg
* Auto-create groups for container/networks created via docker-compose
* Always up-to-date: The underlying data comes directly from the docker host and is retrieved each time the image URL is called
* direct browser call via HTTP
* (proudly made with [golang](https://golang.org) and [plantuml](http://plantuml.com))

Funny fact: The image is an SVG, so you can search for images/container names using your browser's text search.

## Usage
It is really simple to use this app:
```bash
docker run -p 1234:1234 -v /var/run/docker.sock:/var/run/docker.sock:ro bosix/docker-network-visualizer
```
You can now open your browser and go to http://localhost:1234. The visualizer always creates an image with the current data from the Docker API.

## Endpoints

| URL           | description                                                  |
| ------------- | ------------------------------------------------------------ |
| ```/```       | get the visualized docker container environment as svg image |
| ```/list```   | get a list of all containers for a better overview and easier access to VIRTUAL_HOST links |
| ```/source``` | get the plantuml source code of the created image            |
| ```/status``` | get status of the docker-network-visualizer                  |

## Configuration

You can configure the app via several env-parameters and pass them via ```-e```-flag or ```enrironment```-section of docker-compose. All parameters are optional

| name                       | description                                                  | default      | example                                 |
| -------------------------- | ------------------------------------------------------------ | ------------ | --------------------------------------- |
| ROOT_NODE_CONTAINER        | Set the root node (container name). Needed for buiding the container-network-tree correctly. | [empty]      | ```ROOT_NODE_CONTAINER=proxy_nginx_1``` |
| MAIN_NETWORK               | If your root node has multiple networks, it may be useful to set the main network. The main network is used as the parent network in the tree. | [empty]      | ```MAIN_NETWORK=proxy_frontend```       |
| PLANTUML_LIMIT_SIZE        | The image created by the app can be very large if you have many containers running. This env-var changes the max width of the actual picture. If you want to scale down this picture use MAX_WIDTH instead. | 16384 pixels | ```PLANTUML_LIMIT_SIZE=32768```         |
| HORIZONTAL_VIEW            | It is possible to change the orientation of the picture from "up to down" to "left to right". This is useful if you have a lot of containers. | false        | ```HORIZONTAL_VIEW=true```              |
| DOCKER_API_VERSION         | This app uses docker api v1.38 per default. You have to change this if your client is too old. | v1.38        | ```DOCKER_API_VERSION=1.37```           |
| DISABLE_CONTAINER_GROUPING | This app tries to group containers and networks if they are created via docker-compose. You can disable this feature. | false        | ```DISABLE_CONTAINER_GROUPING=true```   |
| MAX_WIDTH                  | You can set a max width of the created output picture.  In contrast to PLANTUML_LIMIT_SIZE this env-var only limits the size of the output image and not the size of the actual created image. | 1800 pixels  | ```MAX_WIDTH=1000```                    |
| DISPLAYED_LABELS           | Comma-separated list of container-labels which will be displayed. | [empty]  | ```DISPLAYED_LABELS=com.mycompany.label1,com.mycompany.label2```                    |

## How it works
The Docker Network Visualizer uses the Docker API (v1.38) to retrieve the necessary data. Please note that mounting the docker socket into a container theoretically allows that container to execute any command as root on your docker host. Check my code to be sure what exactly happens when you run this app.

The data from the Docker API is preprocessed by my little go-app. The app passes the puml-sourcecode to [plantuml](http://plantuml.com/en/) to create an image and sends the result via http to e.g. a browser.

## Examples
If you want to have a look at an example environment, I can recommend you to check out this repo and try out the example:
```bash
./example/start-example.sh
```
You can stop all test-containers created by the start-example.sh by calling:
```bash
./example/stop-example.sh
```


## Copyright
* sourcecode in this repository: MIT ([license](LICENSE))
* golang: BSD ([https://golang.org/LICENSE](https://golang.org/LICENSE))
* plantuml: GNU GPLv3 ([https://github.com/plantuml/plantuml/blob/master/license.txt](https://github.com/plantuml/plantuml/blob/master/license.txt))
