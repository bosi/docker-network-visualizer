package core

import (
	"github.com/allegro/bigcache"
	"time"
)

var config = bigcache.Config{
	Shards:             1024,
	LifeWindow:         7 * 24 * time.Hour,
	CleanWindow:        time.Hour,
	MaxEntriesInWindow: 1000 * 10 * 60,
	MaxEntrySize:       500,
	Verbose:            false,
	HardMaxCacheSize:   256,
	OnRemove:           nil,
	OnRemoveWithReason: nil,
}

var cache appCache

type appCache struct {
	storage *bigcache.BigCache
}

func init() {

	cacheStorage, initErr := bigcache.NewBigCache(config)
	if initErr != nil {
		panic(initErr)
	}

	cache = appCache{storage: cacheStorage}
}

func (ac appCache) add(key string, value string) error {
	return ac.storage.Set(key, []byte(value))
}

func (ac appCache) get(key string) (string, error) {
	byteValue, err := ac.storage.Get(key)
	return string(byteValue), err
}
