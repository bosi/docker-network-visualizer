package core

import "net/http"

// LoadRoutes registers all routes
func LoadRoutes() {
	http.HandleFunc("/", handleImage)
	http.HandleFunc("/list", handleList)
	http.HandleFunc("/source", handleSource)
	http.HandleFunc("/status", handleStatus)
}
