package core

import (
	"docker-network-visualizer/puml"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

const (
	templatePath      = "./views"
	templateExtension = ".html"
)

var templates *template.Template

func init() {
	dir := templatePath
	if _, err := os.Stat(templatePath); err != nil {
		dir = "../" + dir
	}

	templates = template.Must(template.ParseFiles(
		dir+"/index"+templateExtension,
		dir+"/list"+templateExtension,
	))
}

func handleImage(w http.ResponseWriter, r *http.Request) {
	src, err := puml.CreateSource()

	if err != nil {
		http.Error(w, "Error while creating puml sourcecode: "+err.Error(), http.StatusInternalServerError)
		return
	}

	svg, err := cache.get(src)
	if err != nil {
		srcFile, _ := ioutil.TempFile("", "")
		_, err = srcFile.WriteString(src)

		if err != nil {
			http.Error(w, "Error while writing puml-source-code to file: "+err.Error(), http.StatusInternalServerError)
			return
		}

		pumlLimit := os.Getenv("PLANTUML_LIMIT_SIZE")
		if len(pumlLimit) == 0 {
			pumlLimit = "16384"
		}

		cmd := exec.Command("/usr/bin/java", "-jar", "-DPLANTUML_LIMIT_SIZE="+pumlLimit, "plantuml.jar", "-tsvg", srcFile.Name(), "-o", os.TempDir())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			http.Error(w, "Error while creating picture: "+err.Error(), http.StatusInternalServerError)
			return
		}

		imgFile, err := os.Open(srcFile.Name() + ".svg")
		if err != nil {
			http.Error(w, "Error while opening picture file: "+err.Error(), http.StatusInternalServerError)
			return
		}

		imgBytes, err := ioutil.ReadAll(imgFile)
		if err != nil {
			http.Error(w, "Error while reading file from disk: "+err.Error(), http.StatusInternalServerError)
			return
		}

		if err := os.Remove(srcFile.Name()); err != nil {
			log.Fatalf("error while removing puml-source-file: %s", err.Error())
		}
		if err := os.Remove(imgFile.Name()); err != nil {
			log.Fatalf("error while removing puml-picture-file: %s", err.Error())
		}
		svg = string(imgBytes)
		cache.add(src, svg)
	}

	renderTemplate(w, "index", map[string]interface{}{"SvgImage": template.HTML(svg)})
}

func handleList(w http.ResponseWriter, r *http.Request) {
	builder, err := puml.CreateBuilder()
	if err != nil {
		http.Error(w, "Error while fetching data: "+err.Error(), http.StatusInternalServerError)
		return
	}

	builder.OrderElements()

	type dataStruct struct {
		Name   string
		Links  []string
		Labels []puml.Kv
	}

	var data []dataStruct

	for _, element := range builder.GetElements() {
		if element.GetElementType() == puml.TypeContainer {
			newData := dataStruct{
				Name:   puml.NameFmt(element.GetName()),
				Labels: element.GetDisplayedLabels(),
			}

			container := element.(puml.Container)
			for _, kv := range container.GetKv() {
				if kv.GetKey() == "VIRTUAL_HOST" {
					for _, link := range strings.Split(kv.GetValue(), ",") {
						newData.Links = append(newData.Links, "http://"+strings.TrimSpace(link))
					}
				}
			}

			data = append(data, newData)
		}
	}

	renderTemplate(w, "list", map[string]interface{}{"Data": data})
}

func handleSource(w http.ResponseWriter, r *http.Request) {

	src, err := puml.CreateSource()

	if err != nil {
		http.Error(w, "Error while creating puml sourcecode: "+err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, src)
}

func handleStatus(w http.ResponseWriter, r *http.Request) {
	pumlBin := "available"

	if _, err := os.Stat("plantuml.jar"); err != nil {
		pumlBin = "not available"
	}

	fmt.Fprint(w, "App: running\n")
	fmt.Fprintf(w, "Plantuml binary: %s\n", pumlBin)
}

func renderTemplate(w http.ResponseWriter, view string, data map[string]interface{}) {
	err := templates.ExecuteTemplate(w, view+".html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
