dep: ## fetch all dependencies
	go get -v -t ./...

tools: dep ## fetch useful tools for development
	go get -u golang.org/x/lint/golint
	go get github.com/codegangsta/gin
	go get github.com/smartystreets/goconvey

lint: tools ## lint the files
	${GOPATH}/bin/golint -set_exit_status ./...

build: dep ## build the binary
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v

test: dep ## run all tests
	go test ./...

test-cover: dep ## run tests with code coverage
	go test -cover ./...

test-race: dep ## run tests with race detector
	go test -race ./...

test-benchmark: dep ## run tests with benchmarks
	go test -bench=. ./...

run: dep ## directly run the code (installed golang necessary)
	go run *.go
