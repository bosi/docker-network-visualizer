#!/bin/bash

cd $(dirname ${0})

COMPOSE_PROJECT_NAME=multi2 docker-compose -f docker-compose-3.yml down
COMPOSE_PROJECT_NAME=multi1 docker-compose -f docker-compose-2.yml down
COMPOSE_PROJECT_NAME=single1 docker-compose -f docker-compose-1.yml down
COMPOSE_PROJECT_NAME=single2 docker-compose -f docker-compose-4.yml down
COMPOSE_PROJECT_NAME=proxy docker-compose -f docker-compose-0.yml down

cd ../

docker stop docker-network-visualizer-local