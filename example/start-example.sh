#!/bin/bash

cd $(dirname ${0})

COMPOSE_PROJECT_NAME=proxy docker-compose -f docker-compose-0.yml up -d
COMPOSE_PROJECT_NAME=single1 docker-compose -f docker-compose-1.yml up -d
COMPOSE_PROJECT_NAME=single2 docker-compose -f docker-compose-4.yml up -d
COMPOSE_PROJECT_NAME=multi1 docker-compose -f docker-compose-2.yml up -d
COMPOSE_PROJECT_NAME=multi2 docker-compose -f docker-compose-3.yml up -d

cd ../

docker build -t local/docker-network-visualizer .
docker run -d --rm \
    -p 1234:1234 \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    -e "ROOT_NODE_CONTAINER=proxy_testapp1_1" \
    -e "MAIN_NETWORK=proxy_frontend" \
    --name docker-network-visualizer-local \
    local/docker-network-visualizer

echo "-------------------------------------------------------------"
echo "You can now connect to http://127.0.0.1:1234 to view results"
echo "-------------------------------------------------------------"
