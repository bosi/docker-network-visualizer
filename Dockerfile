############################
# STEP 1 build executable binary
############################
FROM golang:1.12 AS builder

WORKDIR $GOPATH/src/docker-network-visualizer

RUN apt-get install git

COPY . .

RUN make build

############################
# STEP 2 build a small image
############################
FROM frolvlad/alpine-java

WORKDIR /app

ENV PLANTUML_VERSION=1.2019.6

RUN \
  apk add --no-cache graphviz wget ca-certificates curl && \
  wget "http://downloads.sourceforge.net/project/plantuml/${PLANTUML_VERSION}/plantuml.${PLANTUML_VERSION}.jar" -O plantuml.jar && \
  apk del wget ca-certificates

ENV LANG en_US.UTF-8

COPY --from=builder /go/src/docker-network-visualizer/docker-network-visualizer /app/
COPY . /app/

HEALTHCHECK --interval=30s \
            --timeout=30s \
            --start-period=5s \
            CMD curl -f http://127.0.0.1:1234/status || exit 1

EXPOSE 1234

ENTRYPOINT ["./docker-network-visualizer"]
