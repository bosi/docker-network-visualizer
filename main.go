package main

import (
	"docker-network-visualizer/core"
	"log"
	"net/http"
)

func main() {
	core.LoadRoutes()
	log.Fatal(http.ListenAndServe(":1234", nil))
}
