package puml

import (
	"os"
	"sort"
	"strconv"
	"strings"
)

// Element is the interface for defining createable elements
type Element interface {
	getPuml(bool) string
	GetName() string
	GetKv() []Kv
	GetElementType() string
	GetLabels() map[string]string
	GetGroupName() string
	IsGrouped() bool
	GetDisplayedLabels() []Kv
}

// Builder represents a worker for holding data to create puml source code
type Builder struct {
	elements          []Element
	createdNetworks   []string
	createdContainers []string
	groupSizes        map[string]int
}

// NewBuilder is the constructor for Builder
func NewBuilder() *Builder {
	return &Builder{
		elements:   []Element{},
		groupSizes: map[string]int{},
	}
}

// GetElements returns all elements added to the builder
func (b *Builder) GetElements() []Element {
	return b.elements
}

// AddElement adds an element to the list of elements which will be used to create the puml source code
func (b *Builder) AddElement(element Element) {
	b.elements = append(b.elements, element)

	if element.IsGrouped() {
		b.groupSizes[element.GetGroupName()] ++
	}

	switch element.GetElementType() {
	case TypeContainer:
		b.createdContainers = append(b.createdContainers, NameFmt(element.GetName()))
	case TypeNetwork:
		b.createdNetworks = append(b.createdNetworks, NameFmt(element.GetName()))
	}
}

// Build creates the puml source code out of all added elements
// It will set color settings, direction etc.
func (b *Builder) Build() string {
	puml := "@startuml\n"

	if os.Getenv("HORIZONTAL_VIEW") == "true" {
		puml = puml + "left to right direction\n"
	}

	puml = puml + "skinparam interface {\n" +
		"backgroundColor GreenYellow\n" +
		"borderColor Green\n" +
		"}\n"

	puml = puml + "skinparam cloud {\n" +
		"backgroundColor #E0E6F8\n" +
		"borderColor #1111F6\n" +
		"}\n"

	puml = puml + "skinparam component {\n" +
		"ArrowColor #424242\n" +
		"}\n"

	puml = puml + "skinparam rectangle {\n" +
		"backgroundColor GhostWhite\n" +
		"borderColor DimGrey\n" +
		"}\n"

	puml = puml + "skinparam database {\n" +
		"backgroundColor #F5ECCE\n" +
		"borderColor #DA8A00\n" +
		"}\n"

	puml = puml + "skinparam svgLinkTarget _blank\n"

	maxWidth := "1800"
	if osWidth := os.Getenv("MAX_WIDTH"); len(osWidth) > 0 {
		if _, err := strconv.Atoi(osWidth); err == nil {
			maxWidth = osWidth
		}
	}

	puml = puml + "scale max " + maxWidth + " width\n"

	b.OrderElements()

	puml += b.getPumlByType(TypeContainer)
	puml += b.getPumlByType(TypeNetwork)
	puml += b.getPumlByType(TypeLink)

	puml = puml + "@enduml\n"
	return puml
}

// NetworkExists checks if a network exists (is already added to builder) by a given name
func (b *Builder) NetworkExists(name string) bool {
	for _, createdNetwork := range b.createdNetworks {
		if createdNetwork == name {
			return true
		}
	}

	return false
}

// ContainerExists checks if a container exists (is already added to builder) by a given name
func (b *Builder) ContainerExists(name string) bool {
	for _, createdContainer := range b.createdContainers {
		if createdContainer == name {
			return true
		}
	}

	return false
}

// OrderElements orders all puml elements by name
func (b *Builder) OrderElements() {
	sort.SliceStable(b.elements, func(i, j int) bool {
		nameE1 := NameFmt(b.elements[i].GetName())
		nameE2 := NameFmt(b.elements[j].GetName())
		return nameE1 <= nameE2
	})
}

func normalize(s string) string {
	return NameFmt(strings.ReplaceAll(s, "-", "__"))
}

func (b *Builder) getPumlByType(elementType string) string {
	puml := ""

	for _, element := range b.elements {
		if element.GetElementType() == elementType {
			asGroup := false
			if element.IsGrouped() {
				asGroup = b.groupSizes[element.GetGroupName()] > 1
			}

			puml = puml + element.getPuml(asGroup)
		}
	}
	return puml
}
