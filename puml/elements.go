package puml

import (
	"os"
	"strconv"
	"strings"
)

const (
	// TypeContainer defines the elementType const for containers
	TypeContainer = "container"
	// TypeNetwork defines the elementType const for networks
	TypeNetwork = "network"
	// TypeLink defines the elementType const for links
	TypeLink = "link"
)

// +++ baseElement ++++++++++
type base struct {
	name        string
	elementType string
	kv          []Kv
	labels      map[string]string
}

func (e base) GetName() string {
	return e.name
}

func (e base) GetElementType() string {
	return e.elementType
}

func (e base) GetKv() []Kv {
	return e.kv
}

func (e base) GetLabels() map[string]string {
	return e.labels
}

func (e base) GetGroupName() string {
	groupName, ok := e.GetLabels()["com.docker.compose.project"]
	if !ok {
		groupName, _ = e.GetLabels()["com.docker.stack.namespace"]
	}

	return groupName
}

func (e base) IsGrouped() bool {
	return len(e.GetGroupName()) > 0
}

func (e base) getPuml(_ bool) string {
	return ""
}

func (e base) GetDisplayedLabels() []Kv {
	labels := []Kv{}
	for key, value := range e.GetLabels() {
		if strings.Contains(os.Getenv("DISPLAYED_LABELS"), key) {
			labels = append(labels, NewKv(key, value))
		}
	}
	return labels
}

// +++ container ++++++++++

// Container represents a docker container
type Container struct {
	base
	ports []string
}

// NewContainer is the constructor for Container
func NewContainer(name string, kv []Kv, ports []string, labels map[string]string) Container {
	return Container{
		base: base{
			name:        name,
			elementType: TypeContainer,
			kv:          kv,
			labels:      labels,
		},
		ports: ports,
	}
}

func (c Container) getPuml(asGroup bool) string {
	containerName := normalize(c.name)
	elementType := "rectangle"

	if strings.Contains(containerName, "mysql") ||
		strings.Contains(containerName, "db") {
		elementType = "database"
	}

	code := elementFmt(c, elementType, asGroup)

	for index, port := range c.ports {
		portName := containerName + "_port_" + strconv.Itoa(index)
		code = code + "interface \"" + port + "\" as " + portName + "\n"
		code = code + "[" + normalize(c.name) + "] .up. " + portName + "\n"
	}

	return code
}

// +++ network ++++++++++

// Network represents a docker network
type Network struct {
	base
}

// NewNetwork is the constructor for Network
func NewNetwork(name string, kv []Kv, labels map[string]string) Network {
	return Network{
		base: base{
			name:        name,
			elementType: TypeNetwork,
			kv:          kv,
			labels:      labels,
		},
	}
}

func (c Network) getPuml(asGroup bool) string {
	return elementFmt(c, "cloud", asGroup)
}

// +++ link ++++++++++

// Link represents a connection between two containers/networks
type Link struct {
	base
	from        string
	to          string
	reverse     bool
	description string
}

// NewLink is the constructor for Link
func NewLink(from string, to string, reverse bool, description string) Link {
	return Link{
		base: base{
			name:        from + "___" + to,
			elementType: TypeLink,
		},
		from:        from,
		to:          to,
		reverse:     reverse,
		description: description,
	}
}

func (l Link) getPuml(_ bool) string {
	var direction string

	if l.reverse {
		direction = "up"
	} else {
		direction = "down"
	}

	puml := normalize(l.from) + " -" + direction + "- " + normalize(l.to)

	if len(l.description) > 0 {
		puml = puml + ": " + l.description
	}

	return puml + "\n"
}

// ReverseDirection inverts the direction of the link
func (l *Link) ReverseDirection() {
	l.reverse = !l.reverse
}
