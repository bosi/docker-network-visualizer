package puml

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestNewBuilder(t *testing.T) {
	builder := NewBuilder()
	assert.IsType(t, &Builder{}, builder)
	assert.Empty(t, builder.elements)
}

func TestBuilder_GetElements(t *testing.T) {
	container := NewContainer("testContainer", []Kv{}, []string{}, map[string]string{})
	network := NewNetwork("testNetwork", []Kv{}, map[string]string{})

	builder := NewBuilder()
	builder.AddElement(container)
	builder.AddElement(network)

	elements := builder.GetElements()
	assert.Equal(t, "testContainer", elements[0].GetName())
	assert.Equal(t, "testNetwork", elements[1].GetName())
	assert.Len(t, builder.elements, 2)
}

func TestBuilder_AddElement(t *testing.T) {
	container := NewContainer("testContainer", []Kv{}, []string{}, map[string]string{})
	network := NewNetwork("testNetwork", []Kv{}, map[string]string{})

	builder := NewBuilder()
	builder.AddElement(container)
	builder.AddElement(network)

	assert.Len(t, builder.elements, 2)

	assert.Equal(t, container.name, builder.createdContainers[0])
	assert.Len(t, builder.createdContainers, 1)

	assert.Equal(t, network.name, builder.createdNetworks[0])
	assert.Len(t, builder.createdNetworks, 1)

	assert.Len(t, builder.groupSizes, 0)

	anotherContainer := NewContainer("anotherTestContainer", []Kv{}, []string{}, map[string]string{"com.docker.compose.project": "testGroup"})
	builder.AddElement(anotherContainer)
	assert.Len(t, builder.groupSizes, 1)
	assert.Equal(t, 1, builder.groupSizes["testGroup"])
}

func TestBuilder_Build(t *testing.T) {
	container := NewContainer("testContainer", []Kv{}, []string{}, map[string]string{})
	network := NewNetwork("testNetwork", []Kv{}, map[string]string{})

	builder := NewBuilder()
	builder.AddElement(container)
	builder.AddElement(network)

	puml := builder.Build()
	assert.Contains(t, puml, "@startuml")
	assert.Contains(t, puml, "@enduml")

	assert.Contains(t, puml, "skinparam interface")
	assert.Contains(t, puml, "skinparam cloud")
	assert.Contains(t, puml, "skinparam component")
	assert.Contains(t, puml, "skinparam rectangle")
	assert.Contains(t, puml, "skinparam database")
	assert.Contains(t, puml, "skinparam svgLinkTarget")

	assert.Contains(t, puml, "cloud testNetwork")
	assert.Contains(t, puml, "rectangle testContainer")
	assert.Contains(t, puml, "scale max 1800 width")

	assert.NotContains(t, puml, "left to right direction")

	os.Setenv("HORIZONTAL_VIEW", "true")
	puml = builder.Build()
	assert.Contains(t, puml, "left to right direction")

	os.Setenv("MAX_WIDTH", "1234")
	puml = builder.Build()
	assert.Contains(t, puml, "scale max 1234 width")

	os.Setenv("MAX_WIDTH", "5678abc")
	puml = builder.Build()
	assert.Contains(t, puml, "scale max 1800 width")
}

func TestBuilder_NetworkExists(t *testing.T) {
	network1 := NewNetwork("testNetwork1", []Kv{}, map[string]string{})
	network2 := NewNetwork("testNetwork2", []Kv{}, map[string]string{})

	builder := NewBuilder()
	builder.AddElement(network1)

	assert.True(t, builder.NetworkExists(network1.name))
	assert.False(t, builder.NetworkExists(network2.name))
}

func TestBuilder_ContainerExists(t *testing.T) {
	container1 := NewContainer("testContainer1", []Kv{}, []string{}, map[string]string{})
	container2 := NewContainer("testContainer2", []Kv{}, []string{}, map[string]string{})

	builder := NewBuilder()
	builder.AddElement(container1)

	assert.True(t, builder.ContainerExists(container1.name))
	assert.False(t, builder.ContainerExists(container2.name))
}

func TestNormalize(t *testing.T) {
	assert.Equal(t, "test__name", normalize("test-name"))
	assert.Equal(t, "test__name", normalize("/test-name"))
	assert.Equal(t, "test_name", normalize("test_name"))
}

func TestBuilder_GetPumlByType(t *testing.T) {
	b := Builder{
		elements: []Element{
			NewContainer("test1", []Kv{}, []string{}, map[string]string{}),
			NewContainer("test2", []Kv{}, []string{}, map[string]string{"com.docker.compose.project": "fancyGroup1"}),
			NewContainer("test3", []Kv{}, []string{}, map[string]string{"com.docker.compose.project": "fancyGroup2"}),
			NewContainer("test4", []Kv{}, []string{}, map[string]string{"com.docker.compose.project": "fancyGroup3"}),
			NewNetwork("test5", []Kv{}, map[string]string{}),
		},
		groupSizes: map[string]int{
			"fancyGroup1": 2,
			"fancyGroup2": 1,
		},
	}

	puml := b.getPumlByType(TypeContainer)
	assert.Contains(t, puml, "test1")
	assert.Contains(t, puml, "test2")
	assert.Contains(t, puml, "test3")
	assert.Contains(t, puml, "test4")
	assert.NotContains(t, puml, "test5")

	assert.Contains(t, puml, "fancyGroup1")
	assert.NotContains(t, puml, "fancyGroup2")
	assert.NotContains(t, puml, "fancyGroup3")
}
