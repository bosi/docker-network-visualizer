package puml

import (
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
	"os"
	"strings"
)

type pumlContext struct {
	builder        *Builder
	containerData  map[string]types.ContainerJSON
	networkData    map[string]types.NetworkResource
	startContainer string
	mainNetwork    string
}

// CreateBuilder creates a builder and fetches all container and network data from the docker api
func CreateBuilder() (*Builder, error) {
	ctx := pumlContext{
		builder:        NewBuilder(),
		containerData:  map[string]types.ContainerJSON{},
		networkData:    map[string]types.NetworkResource{},
		startContainer: os.Getenv("ROOT_NODE_CONTAINER"),
		mainNetwork:    os.Getenv("MAIN_NETWORK"),
	}

	if os.Getenv("DOCKER_API_VERSION") == "" {
		os.Setenv("DOCKER_API_VERSION", "1.38")
	}

	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		return nil, fmt.Errorf("create new client: %s", err.Error())
	}

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		return nil, fmt.Errorf("fetch container list: %s", err.Error())
	}

	networks, err := cli.NetworkList(context.Background(), types.NetworkListOptions{})
	if err != nil {
		return nil, fmt.Errorf("fetch network list: %s", err.Error())
	}

	for _, container := range containers {
		containerDatum, err := cli.ContainerInspect(context.Background(), container.ID)

		if err != nil {
			return nil, fmt.Errorf("get container inspect of \"%s\": %s", container.ID, err.Error())
		}

		ctx.containerData[NameFmt(containerDatum.Name)] = containerDatum
	}

	for _, network := range networks {
		networkDatum, err := cli.NetworkInspect(context.Background(), network.ID, types.NetworkInspectOptions{})

		if err != nil {
			return nil, fmt.Errorf("get network inspect of \"%s\": %s", network.ID, err.Error())
		}

		ctx.networkData[NameFmt(networkDatum.Name)] = networkDatum
	}

	if len(ctx.startContainer) > 0 {
		if err := processContainer(ctx.startContainer, &ctx); err != nil {
			return nil, err
		}
	}

	for _, containerDatum := range ctx.containerData {
		containerName := NameFmt(containerDatum.Name)
		if ctx.builder.ContainerExists(containerName) == false {
			if err := processContainer(containerName, &ctx); err != nil {
				return nil, err
			}
		}
	}

	return ctx.builder, nil
}

// CreateSource puml source from docker api
func CreateSource() (string, error) {
	builder, err := CreateBuilder()
	if err != nil {
		return "", err
	}

	return builder.Build(), nil
}

func processContainer(containerName string, ctx *pumlContext) error {
	if strings.Contains(containerName, "-endpoint") {
		return nil
	}

	containerDatum, ok := ctx.containerData[containerName]

	if ok == false {
		return fmt.Errorf("container with name \"%s\" is not running", containerName)
	}

	var kv []Kv
	kv = append(kv, NewKv("IMAGE", containerDatum.Config.Image))

	for _, env := range containerDatum.Config.Env {
		envSplit := strings.Split(env, "=")

		if len(envSplit) < 2 {
			continue
		}

		envKey := envSplit[0]
		envValue := envSplit[1]

		switch envKey {
		case
			"VIRTUAL_HOST",
			"VIRTUAL_PORT",
			"LETSENCRYPT_HOST":
			kv = append(kv, NewKv(envKey, envValue))
		}
	}

	var ports []string
	for internal, external := range containerDatum.NetworkSettings.Ports {
		if len(external) > 0 {
			text := external[0].HostIP + ":" + external[0].HostPort + "->" + internal.Port() + "/" + internal.Proto()
			ports = append(ports, text)
		}
	}

	ctx.builder.AddElement(NewContainer(
		containerDatum.Name,
		kv,
		ports,
		containerDatum.Config.Labels,
	))

	return processNetworks(containerDatum, ctx)
}

func processNetworks(containerDatum types.ContainerJSON, ctx *pumlContext) error {
	networks := containerDatum.NetworkSettings.Networks
	for name, settings := range networks {
		networkDatum := ctx.networkData[name]

		link := NewLink(containerDatum.Name, name, false, settings.IPAddress)

		if ctx.builder.NetworkExists(name) == false {
			var kv []Kv

			if len(networkDatum.IPAM.Config) > 0 {
				kv = []Kv{NewKv("SUBNET", networkDatum.IPAM.Config[0].Subnet)}
			}

			ctx.builder.AddElement(
				NewNetwork(
					name,
					kv,
					networkDatum.Labels,
				),
			)

		} else if ctx.mainNetwork == name || networkDatum.Ingress || len(networks) == 1 {
			link.ReverseDirection()
		}

		ctx.builder.AddElement(link)

		for _, networkContainer := range ctx.networkData[name].Containers {
			if ctx.builder.ContainerExists(networkContainer.Name) == false {
				if len(ctx.mainNetwork) == 0 {
					ctx.mainNetwork = name
				}

				if err := processContainer(networkContainer.Name, ctx); err != nil {
					return err
				}
			}
		}
	}

	return nil
}
