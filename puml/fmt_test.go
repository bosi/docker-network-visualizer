package puml

import (
	"github.com/stretchr/testify/assert"
	"os"
	"syreclabs.com/go/faker"
	"testing"
)

func TestNewKv(t *testing.T) {
	testKey := "abc123"
	testValue := "valueandsoon"

	kv := NewKv(testKey, testValue)
	assert.Equal(t, testKey, kv.key)
	assert.Equal(t, testValue, kv.value)
}

func TestKv_GetKey(t *testing.T) {
	kv := NewKv(faker.Lorem().Word(), faker.Lorem().Word())
	assert.Equal(t, kv.key, kv.GetKey())
}

func TestKv_GetValue(t *testing.T) {
	kv := NewKv(faker.Lorem().Word(), faker.Lorem().Word())
	assert.Equal(t, kv.value, kv.GetValue())
}

func TestKv_Fmt(t *testing.T) {
	testKey := "abc123"
	testValue := "valueandsoon"

	kv := NewKv(testKey, testValue)
	s := kv.fmt()
	assert.Contains(t, s, testKey)
	assert.Contains(t, s, testValue)
	assert.Contains(t, s, "font size")
}

func TestKvFmt(t *testing.T) {
	kv := []Kv{
		{key: "k1", value: "v1"},
		{key: "k2", value: "v2"},
	}

	assert.Len(t, kvFmt([]Kv{}), 0)

	puml := kvFmt(kv)
	assert.Contains(t, puml, kv[0].key)
	assert.Contains(t, puml, kv[0].value)
	assert.Contains(t, puml, kv[1].key)
	assert.Contains(t, puml, kv[1].value)
}

func TestNameFmt(t *testing.T) {
	assert.Equal(t, "name", NameFmt("/name"))
}

func TestHeadFmt(t *testing.T) {
	testName := "abc123"
	testNameWithSlash := "/" + testName

	assert.Contains(t, headFmt(testNameWithSlash), testName)
	assert.NotContains(t, headFmt(testNameWithSlash), testNameWithSlash)
}

func TestElementFmt(t *testing.T) {
	e := base{
		name:        "myelement",
		elementType: "iamanicetype",
		kv: []Kv{
			{key: "k1", value: "v1"},
			{key: "k2", value: "v2"},
		},
		labels: map[string]string{
			"label1": "labelValue1",
			"label2": "labelValue2",
		},
	}
	eType := "thetype"

	assert.Nil(t, os.Setenv("DISPLAYED_LABELS", ""))
	assert.Nil(t, os.Setenv("DISABLE_CONTAINER_GROUPING", ""))

	puml := elementFmt(e, eType, false)
	assert.NotContains(t, puml, "package \""+e.name)
	assert.Contains(t, puml, eType+" "+e.name)
	assert.Contains(t, puml, e.kv[0].key)
	assert.Contains(t, puml, e.kv[0].value)
	assert.Contains(t, puml, e.kv[1].key)
	assert.Contains(t, puml, e.kv[1].value)
	assert.NotContains(t, puml, "[[")
	assert.NotContains(t, puml, "label")

	e.kv = []Kv{{key: "VIRTUAL_HOST", value: "iamahost"}}
	puml = elementFmt(e, eType, false)
	assert.Contains(t, puml, "[[http://"+e.kv[0].value+"]]")

	e.kv = []Kv{{key: "VIRTUAL_HOST", value: "iamahost, andmetoo"}}
	puml = elementFmt(e, eType, false)
	assert.Contains(t, puml, "[[http://iamahost]]")
	assert.NotContains(t, puml, "[[http://"+e.kv[0].value+"]]")

	projectName := faker.RandomString(5)
	e.labels["com.docker.compose.project"] = projectName
	puml = elementFmt(e, eType, true)
	assert.Contains(t, puml, "package \""+projectName)

	assert.Nil(t, os.Setenv("DISABLE_CONTAINER_GROUPING", "true"))
	puml = elementFmt(e, eType, true)
	assert.NotContains(t, puml, "package \""+projectName)

	puml = elementFmt(e, eType, false)
	assert.NotContains(t, puml, "package \""+projectName)

	assert.Nil(t, os.Setenv("DISPLAYED_LABELS", "label1"))
	puml = elementFmt(e, eType, false)
	assert.Contains(t, puml, "labelValue1")
}
