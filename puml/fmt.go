package puml

import (
	"os"
	"strings"
)

// Kv is a struct for storing a key-value-pair
type Kv struct {
	key   string
	value string
}

// NewKv is the constructor for Kv
func NewKv(key string, value string) Kv {
	return Kv{
		key:   key,
		value: value,
	}
}

// GetKey returns the key of the key-value-pair
func (kv Kv) GetKey() string {
	return kv.key
}

// GetValue returns the value of the key-value-pair
func (kv Kv) GetValue() string {
	return kv.value
}

func (kv Kv) fmt() string {
	return "<font size=\"10\"><b>" + kv.key + ":</b> " + kv.value + "</font>\n"
}

func kvFmt(kv []Kv) string {
	puml := ""
	for _, kv := range kv {
		puml = puml + kv.fmt()
	}

	return puml
}

// NameFmt formats a name for a container/network/etc.
func NameFmt(name string) string {
	return strings.TrimPrefix(name, "/")
}

func headFmt(name string) string {
	return "<b>" + NameFmt(name) + "</b>\n" +
		"---\n"
}

func elementFmt(e Element, elementType string, asGroup bool) string {
	link := ""
	linkCode := ""
	for _, kv := range e.GetKv() {
		if kv.key == "VIRTUAL_HOST" {
			link = "http://" + kv.value
			link = strings.Split(link, ",")[0]

			linkCode = "[[" + link + "]]"
			link = "<u><color:blue><font size=\"12\">" + link + "</font></color></u>\n" +
				"---\n"
		}
	}

	labels := e.GetDisplayedLabels()

	labelsPuml := ""
	if len(labels) > 0 {
		labelsPuml += "---\n"
		labelsPuml += kvFmt(labels)
	}

	puml := elementType + " " + normalize(e.GetName()) + " " + linkCode + " [\n"
	puml += headFmt(e.GetName())
	puml += link
	puml += kvFmt(e.GetKv())
	puml += labelsPuml
	puml += "]\n"

	if os.Getenv("DISABLE_CONTAINER_GROUPING") != "true" {
		if e.IsGrouped() && strings.Contains(e.GetGroupName(), "proxy") == false && asGroup {
			puml = "package \"" + e.GetGroupName() + "\" {\n" + puml
			puml += "}\n"
		}
	}

	return puml
}
