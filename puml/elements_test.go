package puml

import (
	"github.com/stretchr/testify/assert"
	"os"
	"syreclabs.com/go/faker"
	"testing"
)

func TestBase_GetName(t *testing.T) {
	name := faker.Name().FirstName()

	b := base{name: name}
	assert.Equal(t, name, b.GetName())
}

func TestBase_GetElementType(t *testing.T) {
	elementType := faker.RandomString(10)

	b := base{elementType: elementType}
	assert.Equal(t, elementType, b.GetElementType())
}

func TestBase_GetKv(t *testing.T) {
	kv := []Kv{{key: faker.RandomString(5), value: faker.RandomString(5)}}

	b := base{kv: kv}
	assert.Equal(t, kv, b.GetKv())
}

func TestBase_GetLabels(t *testing.T) {
	testLabels := map[string]string{
		faker.Lorem().Word(): faker.Lorem().Word(),
		faker.Lorem().Word(): faker.Lorem().Word(),
	}

	base := base{labels: testLabels}
	assert.Equal(t, testLabels, base.GetLabels())
}

func TestBase_GetGroupName(t *testing.T) {
	dcGroup := faker.Lorem().Word()
	dsGroup := faker.Lorem().Word()

	assert.Equal(t, dcGroup, base{labels: map[string]string{"com.docker.compose.project": dcGroup}}.GetGroupName())
	assert.Equal(t, dsGroup, base{labels: map[string]string{"com.docker.stack.namespace": dsGroup}}.GetGroupName())
}

func TestBase_IsGrouped(t *testing.T) {
	assert.False(t, base{}.IsGrouped())
	assert.True(t, base{labels: map[string]string{"com.docker.compose.project": faker.Lorem().Word()}}.IsGrouped())
}

func TestBase_GetDisplayedLabels(t *testing.T) {
	label1 := faker.Lorem().Word()
	label2 := faker.Lorem().Word()

	assert.Nil(t, os.Setenv("DISPLAYED_LABELS", "com.test.t1, com.test.t3"))
	labels := base{labels: map[string]string{"com.test.t1": label1, "com.test.t2": label2}}.GetDisplayedLabels()

	assert.Len(t, labels, 1)
	assert.Equal(t, label1, labels[0].value)
}

func TestNewContainer(t *testing.T) {
	name := faker.Name().Name()
	kv := []Kv{{key: faker.RandomString(5), value: faker.RandomString(5)}}
	ports := []string{faker.RandomString(5), faker.RandomString(5)}
	labels := map[string]string{faker.RandomString(5): faker.RandomString(5)}

	c := NewContainer(name, kv, ports, labels)
	assert.IsType(t, Container{}, c)
	assert.Equal(t, name, c.name)
	assert.Equal(t, kv, c.kv)
	assert.Equal(t, ports, c.ports)
	assert.Equal(t, labels, c.labels)
	assert.Equal(t, TypeContainer, c.elementType)
}

func TestContainer_GetPuml(t *testing.T) {
	tests := []struct {
		name    string
		c       Container
		see     []string
		dontSee []string
	}{
		{
			name:    "happy case",
			c:       NewContainer("my_name", []Kv{{key: "k1", value: "v1"}}, []string{"port1", "port2"}, map[string]string{}),
			see:     []string{"rectangle", "port1", "port2", "k1", "v1"},
			dontSee: []string{"database"},
		},
		{
			name:    "with \"mysql\" as container-name",
			c:       NewContainer("mysql", []Kv{}, []string{}, map[string]string{}),
			see:     []string{"database"},
			dontSee: []string{"rectangle"},
		},
		{
			name:    "with \"db\" as container-name",
			c:       NewContainer("db", []Kv{}, []string{}, map[string]string{}),
			see:     []string{"database"},
			dontSee: []string{"rectangle"},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			puml := test.c.getPuml(true)

			for _, see := range test.see {
				assert.Contains(t, puml, see)
			}

			for _, dontSee := range test.dontSee {
				assert.NotContains(t, puml, dontSee)
			}
		})
	}
}

func TestNewNetwork(t *testing.T) {
	name := faker.Name().Name()
	kv := []Kv{{key: faker.RandomString(5), value: faker.RandomString(5)}}

	n := NewNetwork(name, kv, map[string]string{})
	assert.IsType(t, Network{}, n)
	assert.Equal(t, name, n.name)
	assert.Equal(t, kv, n.kv)
	assert.Equal(t, TypeNetwork, n.elementType)
}

func TestNetwork_GetPuml(t *testing.T) {
	n := NewNetwork(faker.RandomString(5), []Kv{{key: faker.RandomString(5), value: faker.RandomString(5)}}, map[string]string{})

	puml := n.getPuml(true)
	assert.Contains(t, puml, "cloud")
	assert.Contains(t, puml, n.name)
	assert.Contains(t, puml, n.kv[0].key)
	assert.Contains(t, puml, n.kv[0].value)
}

func TestNewLink(t *testing.T) {
	from := faker.Name().Name()
	to := faker.Name().Name()
	reverse := faker.Number().Between(0, 1) == "0"
	description := faker.Lorem().Word()

	l := NewLink(from, to, reverse, description)
	assert.IsType(t, Link{}, l)
	assert.Equal(t, from, l.from)
	assert.Equal(t, to, l.to)
	assert.Equal(t, reverse, l.reverse)
	assert.Equal(t, description, l.description)
	assert.Equal(t, from+"___"+to, l.base.name)
	assert.Equal(t, TypeLink, l.base.elementType)
}

func TestLink_GetPuml(t *testing.T) {
	tests := []struct {
		name   string
		l      Link
		expect string
	}{
		{
			name:   "normal without description",
			l:      NewLink("p1", "p2", false, ""),
			expect: "p1 -down- p2\n",
		},
		{
			name:   "reverse without description",
			l:      NewLink("p1", "p2", true, ""),
			expect: "p1 -up- p2\n",
		},
		{
			name:   "normal with description",
			l:      NewLink("p1", "p2", false, "my description"),
			expect: "p1 -down- p2: my description\n",
		},
		{
			name:   "reverse with description",
			l:      NewLink("p1", "p2", true, "my fancy description"),
			expect: "p1 -up- p2: my fancy description\n",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			puml := test.l.getPuml(true)

			assert.Equal(t, test.expect, puml)
		})
	}
}

func TestLink_ReverseDirection(t *testing.T) {
	l := Link{reverse: true}
	l.ReverseDirection()
	assert.False(t, l.reverse)
	l.ReverseDirection()
	assert.True(t, l.reverse)
}
